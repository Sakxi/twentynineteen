<?php 

function alecaddd_gutenberg_blocks()
{
    wp_register_script( 'custom-cta-js', get_template_directory_uri() . '/build/index.js', array( 'wp-blocks' ));

    register_block_type( 'alecaddd/custom-cta', array(
        'editor_script' => 'custom-cta-js'
    ) );
}
add_action( 'init', 'alecaddd_gutenberg_blocks' );